export default [
  {
    name: "Plastic",
    symbol: "SE",
    number: 22,
    weight: 90,
  },
  {
    name: "Glass",
    symbol: "HR",
    number: 44,
    weight: 24,
  },
  {
    name: "Brass",
    symbol: "PL",
    number: 57,
    weight: 40,
  },
  {
    name: "Wood",
    symbol: "CA",
    number: 30,
    weight: 90,
  },
  {
    name: "Aluminum",
    symbol: "CO",
    number: 71,
    weight: 23,
  },
  {
    name: "Vinyl",
    symbol: "JP",
    number: 31,
    weight: 42,
  },
  {
    name: "Wood",
    symbol: "RU",
    number: 20,
    weight: 74,
  },
  {
    name: "Brass",
    symbol: "YE",
    number: 93,
    weight: 75,
  },
  {
    name: "Rubber",
    symbol: "CN",
    number: 45,
    weight: 22,
  },
  {
    name: "Steel",
    symbol: "BY",
    number: 35,
    weight: 52,
  },
];
