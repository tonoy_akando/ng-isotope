export interface DataElement {
  name:   string;
  symbol: string;
  number: number;
  weight: number;
}
