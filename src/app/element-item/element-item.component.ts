import { Component, Input, OnInit } from "@angular/core";
import { DataElement } from "../data/element";

@Component({
  selector: "app-element-item",
  templateUrl: "./element-item.component.html",
  styleUrls: ["./element-item.component.scss"],
})
export class ElementItemComponent implements OnInit {
  @Input() dataElement: DataElement;
  constructor() {}

  ngOnInit(): void {}
}
