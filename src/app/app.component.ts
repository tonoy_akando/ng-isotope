import { AfterViewInit, Component } from "@angular/core";
import InfiniteScroll from "infinite-scroll";
import data from "./data/data";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements AfterViewInit {
  infiniteScroll: any;
  items = data;
  count = 1;
  ngAfterViewInit(): void {
    this.infiniteScroll = new InfiniteScroll(".grid", {
      path: () => `https://jsonplaceholder.typicode.com/todos/${this.count}`,
      responseBody: "json",
      status: ".page-load-status",
      history: false,
    });
    this.infiniteScroll.on("load", (body: any) => {
      console.log("Loading...", body);
      this.items = [...this.items, ...this.items]
      this.count++;
    });
  }
}
